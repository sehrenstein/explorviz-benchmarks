# adapter isolated
## delete
kubectl delete configmap adapter-isolated-benchmark-lg-configmap
kubectl delete configmap adapter-isolated-benchmark-sut-configmap

## create
kubectl create configmap adapter-isolated-benchmark-lg-configmap --from-file adapter-isolated/resources/workload-generator
kubectl create configmap adapter-isolated-benchmark-sut-configmap --from-file adapter-isolated/resources/sut

# trace isolated
## delete
kubectl delete configmap trace-isolated-benchmark-lg-configmap
kubectl delete configmap trace-isolated-benchmark-sut-configmap

## create
kubectl create configmap trace-isolated-benchmark-lg-configmap --from-file trace-isolated/resources/workload-generator
kubectl create configmap trace-isolated-benchmark-sut-configmap --from-file trace-isolated/resources/sut

# landscape isolated
## delete
kubectl delete configmap landscape-isolated-benchmark-lg-configmap
kubectl delete configmap landscape-isolated-benchmark-sut-configmap

## create
kubectl create configmap landscape-isolated-benchmark-lg-configmap --from-file landscape-isolated/resources/workload-generator
kubectl create configmap landscape-isolated-benchmark-sut-configmap --from-file landscape-isolated/resources/sut

# combined
## delete
kubectl delete configmap combined-benchmark-lg-configmap
kubectl delete configmap combined-benchmark-sut-configmap
kubectl delete configmap combined-benchmark-infrastructure-configmap

## create
kubectl create configmap combined-benchmark-lg-configmap --from-file combined/resources/workload-generator
kubectl create configmap combined-benchmark-sut-configmap --from-file combined/resources/sut
kubectl create configmap combined-benchmark-infrastructure-configmap --from-file combined/resources/infrastructure