{
  "name": "combined-benchmark-instances",
  "resourceTypes": [
    {
      "typeName": "Instances",
      "patchers": [
        {
          "type": "ProportionalReplicaPatcher",
          "resource": "adapter-deployment.yaml",
          "properties": {
            "factor": "2"
          }
        },
        {
          "type": "ProportionalReplicaPatcher",
          "resource": "landscape-deployment.yaml",
          "properties": {
            "factor": "1"
          }
        },
        {
          "type": "ProportionalReplicaPatcher",
          "resource": "trace-deployment.yaml",
          "properties": {
            "factor": "3"
          }
        }
      ]
    }
  ],
  "loadTypes": [
    {
      "typeName": "NumSensors",
      "patchers": [
        {
          "type": "EnvVarPatcher",
          "resource": "workload-generator-span-1-deployment.yaml",
          "properties": {
            "container": "combined-workload-generator-span-1",
            "variableName": "NUM_SENSORS"
          }
        },
        {
          "type": "NumSensorsLoadGeneratorReplicaPatcher",
          "resource": "workload-generator-span-1-deployment.yaml",
          "properties": {
            "loadGenMaxRecords": "40000"
          }
        }
      ]
    }
  ],
  "kafkaConfig": {
    "bootstrapServer": "theodolite-kafka-kafka-bootstrap:9092",
    "topics": [
      {
        "name": "dummy",
        "numPartitions$delegate": {
          "value": 1
        },
        "replicationFactor$delegate": {
          "value": 1
        },
        "removeOnly$delegate": {
          "state": false
        }
      }
    ]
  },
  "infrastructure": {
    "resources": [],
    "beforeActions": [],
    "afterActions": []
  },
  "sut": {
    "resources": [
      {
        "configMap": {
          "name": "combined-benchmark-sut-configmap",
          "files": [
            "cluster-dump-spans-topic.yaml",
            "explorviz-records-topic.yaml",
            "explorviz-spans-dynamic-topic.yaml",
            "explorviz-spans-structure-topic.yaml",
            "explorviz-spans-topic.yaml",
            "explorviz-traces-topic.yaml",
            "token-events-table-topic.yaml",
            "token-events-topic.yaml",
            "adapter-podmonitor.yaml",
            "adapter-deployment.yaml",
            "user-deployment.yaml",
            "configmap.yaml",
            "landscape-deployment.yaml",
            "landscape-podmonitor.yaml",
            "landscape-service.yaml",
            "trace-deployment.yaml",
            "trace-podmonitor.yaml"
          ]
        }
      }
    ],
    "beforeActions": [
      {
        "execCommand": {
          "selector": {
            "pod": {
              "matchLabels": {
                "app.kubernetes.io/name": "mongodb"
              }
            },
            "container": "mongodb"
          },
          "command": [
            "mongo",
            "token",
            "--eval",
            "db.dropDatabase()"
          ],
          "timeoutSeconds": 60
        }
      },
      {
        "execCommand": {
          "selector": {
            "pod": {
              "matchLabels": {
                "app": "cassandra-client"
              }
            },
            "container": "cassandra-client"
          },
          "command": [
            "cqlsh",
            "-u",
            "explorviz",
            "-p",
            "explorviz",
            "-f",
            "/before-action-structure.cql",
            "theodolite-cassandra-structure",
            "9042"
          ],
          "timeoutSeconds": 60
        }
      },
      {
        "execCommand": {
          "selector": {
            "pod": {
              "matchLabels": {
                "app": "cassandra-client"
              }
            },
            "container": "cassandra-client"
          },
          "command": [
            "cqlsh",
            "-u",
            "explorviz",
            "-p",
            "explorviz",
            "-f",
            "/before-action-traces.cql",
            "theodolite-cassandra-traces",
            "9042"
          ],
          "timeoutSeconds": 60
        }
      },
      {
        "deleteCommand": {
          "selector": {
            "apiVersion": "kafka.strimzi.io/v1beta2",
            "kind": "KafkaTopic",
            "nameRegex": "^trace-service-.*"
          }
        }
      }
    ],
    "afterActions": [
      {
        "execCommand": {
          "selector": {
            "pod": {
              "matchLabels": {
                "app.kubernetes.io/name": "mongodb"
              }
            },
            "container": "mongodb"
          },
          "command": [
            "mongo",
            "token",
            "--eval",
            "db.dropDatabase()"
          ],
          "timeoutSeconds": 60
        }
      },
      {
        "execCommand": {
          "selector": {
            "pod": {
              "matchLabels": {
                "app": "cassandra-client"
              }
            },
            "container": "cassandra-client"
          },
          "command": [
            "cqlsh",
            "-u",
            "explorviz",
            "-p",
            "explorviz",
            "-f",
            "/after-action.cql",
            "theodolite-cassandra-structure",
            "9042"
          ],
          "timeoutSeconds": 60
        }
      },
      {
        "execCommand": {
          "selector": {
            "pod": {
              "matchLabels": {
                "app": "cassandra-client"
              }
            },
            "container": "cassandra-client"
          },
          "command": [
            "cqlsh",
            "-u",
            "explorviz",
            "-p",
            "explorviz",
            "-f",
            "/after-action.cql",
            "theodolite-cassandra-traces",
            "9042"
          ],
          "timeoutSeconds": 60
        }
      },
      {
        "deleteCommand": {
          "selector": {
            "apiVersion": "kafka.strimzi.io/v1beta2",
            "kind": "KafkaTopic",
            "nameRegex": "^trace-service-.*"
          }
        }
      }
    ]
  },
  "loadGenerator": {
    "resources": [
      {
        "configMap": {
          "name": "combined-benchmark-lg-configmap",
          "files": [
            "workload-generator-span-1-deployment.yaml"
          ]
        }
      }
    ],
    "beforeActions": [],
    "afterActions": []
  },
  "namespace": "theodolite-stu200776"
}
