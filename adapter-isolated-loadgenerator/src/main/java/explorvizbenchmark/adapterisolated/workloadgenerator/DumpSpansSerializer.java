package explorvizbenchmark.adapterisolated.workloadgenerator;

import com.google.protobuf.Message;
import io.opencensus.proto.dump.DumpSpans;
import org.apache.kafka.common.serialization.Serializer;

public class DumpSpansSerializer implements Serializer<DumpSpans> {
    @Override
    public byte[] serialize(String topic, DumpSpans data) {
        return data.toByteArray();
    }
}
