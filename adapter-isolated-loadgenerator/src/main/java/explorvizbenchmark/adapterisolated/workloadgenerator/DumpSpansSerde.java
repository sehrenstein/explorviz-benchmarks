package explorvizbenchmark.adapterisolated.workloadgenerator;


import io.opencensus.proto.dump.DumpSpans;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serializer;

public class DumpSpansSerde implements Serde<DumpSpans> {
    @Override
    public Serializer<DumpSpans> serializer() {
        return new DumpSpansSerializer();
    }

    @Override
    public Deserializer<DumpSpans> deserializer() {
        return new DumpSpansDeserializer();
    }
}
