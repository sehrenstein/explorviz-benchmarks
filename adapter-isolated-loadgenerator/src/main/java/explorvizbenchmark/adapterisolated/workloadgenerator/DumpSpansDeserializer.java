package explorvizbenchmark.adapterisolated.workloadgenerator;

import com.google.protobuf.InvalidProtocolBufferException;
import io.opencensus.proto.dump.DumpSpans;
import org.apache.kafka.common.errors.SerializationException;
import org.apache.kafka.common.serialization.Deserializer;

public class DumpSpansDeserializer implements Deserializer<DumpSpans> {

    @Override
    public DumpSpans deserialize(String topic, byte[] data) {
        try {
            return DumpSpans.parseFrom(data);
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
            throw new SerializationException("Could not parse message on topic " + topic);
        }
    }
}
