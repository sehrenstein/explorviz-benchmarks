package explorvizbenchmark.adapterisolated.workloadgenerator;

import io.opencensus.proto.dump.DumpSpans;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.errors.SerializationException;
import org.apache.kafka.common.serialization.Serdes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import theodolite.commons.workloadgeneration.KafkaRecordSender;
import theodolite.commons.workloadgeneration.RecordSender;

import java.util.Properties;
import java.util.function.Function;

public class KafkaProtoRecordSender<T extends DumpSpans> implements RecordSender<T> {
    private static final Logger LOGGER = LoggerFactory.getLogger(KafkaProtoRecordSender.class);

    private final String topic;

    private final Function<T, String> keyAccessor;

    private final Function<T, Long> timestampAccessor;

    private final Producer<String, DumpSpans> producer;

    /**
     * Create a new {@link KafkaRecordSender}.
     */
    private KafkaProtoRecordSender(final KafkaProtoRecordSender.Builder<T> builder) {
        this.topic = builder.topic;
        this.keyAccessor = builder.keyAccessor;
        this.timestampAccessor = builder.timestampAccessor;

        final Properties properties = new Properties();
        properties.putAll(builder.defaultProperties);
        properties.put("bootstrap.servers", builder.bootstrapServers);
        properties.put("acks", "all");
        properties.put("enable.idempotence", "true");
        // properties.put("acks", this.acknowledges);
        // properties.put("batch.size", this.batchSize);
        // properties.put("linger.ms", this.lingerMs);
        // properties.put("buffer.memory", this.bufferMemory);

        final DumpSpansSerde serde = new DumpSpansSerde();
        this.producer = new KafkaProducer<>(properties, Serdes.String().serializer(), serde.serializer());
    }

    /**
     * Write the passed monitoring record to Kafka.S
     */
    public void write(final T monitoringRecord) {
        final ProducerRecord<String, DumpSpans> record =
                new ProducerRecord<>(this.topic, null, this.timestampAccessor.apply(monitoringRecord),
                        this.keyAccessor.apply(monitoringRecord), monitoringRecord);

        LOGGER.debug("Send record to Kafka topic {}: {}", this.topic, record);
        try {
            this.producer.send(record);
        } catch (final SerializationException e) {
            LOGGER.warn(
                    "Record could not be serialized and thus not sent to Kafka due to exception. Skipping this record.", // NOCS
                    e);
        }
    }

    public void terminate() {
        this.producer.close();
    }

    @Override
    public void send(final T message) {
        this.write(message);
    }

    public static <T extends DumpSpans> KafkaProtoRecordSender.Builder<T> builder(
            final String bootstrapServers,
            final String topic) {
        return new KafkaProtoRecordSender.Builder<>(bootstrapServers, topic);
    }

    /**
     * Builder class to build a new {@link KafkaRecordSender}.
     *
     * @param <T> Type of the records that should later be send.
     */
    public static class Builder<T extends DumpSpans> {

        private final String bootstrapServers;
        private final String topic;
        private Function<T, String> keyAccessor = x -> ""; // NOPMD
        private Function<T, Long> timestampAccessor = x -> null; // NOPMD
        private Properties defaultProperties = new Properties(); // NOPMD

        /**
         * Creates a Builder object for a {@link KafkaRecordSender}.
         *
         * @param bootstrapServers The Server to for accessing Kafka.
         * @param topic The topic where to write.
         */
        private Builder(final String bootstrapServers, final String topic) {
            this.bootstrapServers = bootstrapServers;
            this.topic = topic;
        }

        public KafkaProtoRecordSender.Builder<T> keyAccessor(final Function<T, String> keyAccessor) {
            this.keyAccessor = keyAccessor;
            return this;
        }

        public KafkaProtoRecordSender.Builder<T> timestampAccessor(final Function<T, Long> timestampAccessor) {
            this.timestampAccessor = timestampAccessor;
            return this;
        }

        public KafkaProtoRecordSender.Builder<T> defaultProperties(final Properties defaultProperties) {
            this.defaultProperties = defaultProperties;
            return this;
        }

        public KafkaProtoRecordSender<T> build() {
            return new KafkaProtoRecordSender<>(this);
        }
    }
}
