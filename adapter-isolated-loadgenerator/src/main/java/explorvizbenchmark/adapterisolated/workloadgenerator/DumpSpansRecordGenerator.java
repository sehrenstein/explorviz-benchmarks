package explorvizbenchmark.adapterisolated.workloadgenerator;

import com.google.protobuf.*;
import io.opencensus.proto.dump.DumpSpans;
import io.opencensus.proto.trace.v1.AttributeValue;
import io.opencensus.proto.trace.v1.Span;
import io.opencensus.proto.trace.v1.TruncatableString;
import org.apache.commons.lang3.RandomStringUtils;
import theodolite.commons.workloadgeneration.RecordGenerator;

import java.util.*;

public class DumpSpansRecordGenerator implements RecordGenerator<DumpSpans> {

    final int traceLength;
    final String landscapeToken;
    final String tokenSecret;

    @Override
    public DumpSpans generate(String key) {
        DumpSpans.Builder builder = DumpSpans.newBuilder();

        Span.Builder spanBuilder = Span.newBuilder();
        final List<Span> spans = new LinkedList<>();
        String lastSpanId = "";
        final String traceId = RandomStringUtils.randomAlphanumeric(24);

        for (int i=0; i<traceLength; i++) {
            // trace id
            spanBuilder.setTraceId(ByteString.copyFromUtf8(traceId));

            // parent span id
            spanBuilder.setParentSpanId(ByteString.copyFromUtf8(lastSpanId));
            lastSpanId = RandomStringUtils.randomAlphanumeric(12);

            // span id
            spanBuilder.setSpanId(ByteString.copyFromUtf8(lastSpanId));

            // name
            spanBuilder.setName(TruncatableString.newBuilder().setValue("method"+i));

            // start time
            long startMillis = System.currentTimeMillis();
            Timestamp startTime = Timestamp.newBuilder().setSeconds(startMillis / 1000)
                    .setNanos((int) ((startMillis % 1000) * 1000000)).build();
            spanBuilder.setStartTime(startTime);

            // end time
            long endMillis = startMillis + 30;
            Timestamp endTime = Timestamp.newBuilder().setSeconds(endMillis / 1000)
                    .setNanos((int) ((endMillis % 1000) * 1000000)).build();
            spanBuilder.setEndTime(startTime);

            // attributes>
            final Map<String, AttributeValue> attributes = new HashMap<>();

            // application instance id
            attributes.put("application_instance_id", AttributeValue.newBuilder().setStringValue(TruncatableString.newBuilder().setValue("0")).build());

            // landscape token
            attributes.put("landscape_token", AttributeValue.newBuilder().setStringValue(TruncatableString.newBuilder().setValue(this.landscapeToken)).build());

            // service
            attributes.put("service", AttributeValue.newBuilder().setStringValue(TruncatableString.newBuilder().setValue("InspectIT Agent")).build());

            // host_address
            attributes.put("host_address", AttributeValue.newBuilder().setStringValue(TruncatableString.newBuilder().setValue("172.24.0.15")).build());

            // application_name
            attributes.put("application_name", AttributeValue.newBuilder().setStringValue(TruncatableString.newBuilder().setValue("load-generator")).build());

            // host
            attributes.put("host", AttributeValue.newBuilder().setStringValue(TruncatableString.newBuilder().setValue("869e0fa6ec45")).build());

            // application_language
            attributes.put("application_language", AttributeValue.newBuilder().setStringValue(TruncatableString.newBuilder().setValue("java")).build());

            // java.fqn
            attributes.put("java.fqn", AttributeValue.newBuilder().setStringValue(TruncatableString.newBuilder().setValue("my.benchmark.package.Main.method"+i)).build());

            // token_secret
            attributes.put("token_secret", AttributeValue.newBuilder().setStringValue(TruncatableString.newBuilder().setValue(this.tokenSecret)).build());

            spanBuilder.setAttributes(Span.Attributes.newBuilder().putAllAttributeMap(attributes));

            // same process as parent
            spanBuilder.setSameProcessAsParentSpan(BoolValue.newBuilder().setValue(true));
            spans.add(spanBuilder.build());
        }

        builder.addAllSpans(spans);

        DumpSpans dumpSpans = builder.build();

        return dumpSpans;
    }

    public DumpSpansRecordGenerator(final int traceLength, final String landScapeToken, final String tokenSecret) {
        this.traceLength = traceLength;
        this.landscapeToken = landScapeToken;
        this.tokenSecret = tokenSecret;
    }
}
