/**
 * Autogenerated by Avro
 *
 * DO NOT EDIT DIRECTLY
 */
package net.explorviz.avro;

import org.apache.avro.generic.GenericArray;
import org.apache.avro.specific.SpecificData;
import org.apache.avro.util.Utf8;
import org.apache.avro.message.BinaryMessageEncoder;
import org.apache.avro.message.BinaryMessageDecoder;
import org.apache.avro.message.SchemaStore;

@org.apache.avro.specific.AvroGenerated
public class SpanDynamic extends org.apache.avro.specific.SpecificRecordBase implements org.apache.avro.specific.SpecificRecord {
  private static final long serialVersionUID = 1974426708576294822L;


  public static final org.apache.avro.Schema SCHEMA$ = new org.apache.avro.Schema.Parser().parse("{\"type\":\"record\",\"name\":\"SpanDynamic\",\"namespace\":\"net.explorviz.avro\",\"fields\":[{\"name\":\"landscapeToken\",\"type\":{\"type\":\"string\",\"avro.java.string\":\"String\"}},{\"name\":\"spanId\",\"type\":{\"type\":\"string\",\"avro.java.string\":\"String\"}},{\"name\":\"parentSpanId\",\"type\":{\"type\":\"string\",\"avro.java.string\":\"String\"}},{\"name\":\"traceId\",\"type\":{\"type\":\"string\",\"avro.java.string\":\"String\"}},{\"name\":\"startTime\",\"type\":{\"type\":\"record\",\"name\":\"Timestamp\",\"fields\":[{\"name\":\"seconds\",\"type\":\"long\"},{\"name\":\"nanoAdjust\",\"type\":\"int\"}]}},{\"name\":\"endTime\",\"type\":\"Timestamp\"},{\"name\":\"hashCode\",\"type\":{\"type\":\"string\",\"avro.java.string\":\"String\"}}]}");
  public static org.apache.avro.Schema getClassSchema() { return SCHEMA$; }

  private static final SpecificData MODEL$ = new SpecificData();

  private static final BinaryMessageEncoder<SpanDynamic> ENCODER =
      new BinaryMessageEncoder<SpanDynamic>(MODEL$, SCHEMA$);

  private static final BinaryMessageDecoder<SpanDynamic> DECODER =
      new BinaryMessageDecoder<SpanDynamic>(MODEL$, SCHEMA$);

  /**
   * Return the BinaryMessageEncoder instance used by this class.
   * @return the message encoder used by this class
   */
  public static BinaryMessageEncoder<SpanDynamic> getEncoder() {
    return ENCODER;
  }

  /**
   * Return the BinaryMessageDecoder instance used by this class.
   * @return the message decoder used by this class
   */
  public static BinaryMessageDecoder<SpanDynamic> getDecoder() {
    return DECODER;
  }

  /**
   * Create a new BinaryMessageDecoder instance for this class that uses the specified {@link SchemaStore}.
   * @param resolver a {@link SchemaStore} used to find schemas by fingerprint
   * @return a BinaryMessageDecoder instance for this class backed by the given SchemaStore
   */
  public static BinaryMessageDecoder<SpanDynamic> createDecoder(SchemaStore resolver) {
    return new BinaryMessageDecoder<SpanDynamic>(MODEL$, SCHEMA$, resolver);
  }

  /**
   * Serializes this SpanDynamic to a ByteBuffer.
   * @return a buffer holding the serialized data for this instance
   * @throws java.io.IOException if this instance could not be serialized
   */
  public java.nio.ByteBuffer toByteBuffer() throws java.io.IOException {
    return ENCODER.encode(this);
  }

  /**
   * Deserializes a SpanDynamic from a ByteBuffer.
   * @param b a byte buffer holding serialized data for an instance of this class
   * @return a SpanDynamic instance decoded from the given buffer
   * @throws java.io.IOException if the given bytes could not be deserialized into an instance of this class
   */
  public static SpanDynamic fromByteBuffer(
      java.nio.ByteBuffer b) throws java.io.IOException {
    return DECODER.decode(b);
  }

  private java.lang.String landscapeToken;
  private java.lang.String spanId;
  private java.lang.String parentSpanId;
  private java.lang.String traceId;
  private net.explorviz.avro.Timestamp startTime;
  private net.explorviz.avro.Timestamp endTime;
  private java.lang.String hashCode;

  /**
   * Default constructor.  Note that this does not initialize fields
   * to their default values from the schema.  If that is desired then
   * one should use <code>newBuilder()</code>.
   */
  public SpanDynamic() {}

  /**
   * All-args constructor.
   * @param landscapeToken The new value for landscapeToken
   * @param spanId The new value for spanId
   * @param parentSpanId The new value for parentSpanId
   * @param traceId The new value for traceId
   * @param startTime The new value for startTime
   * @param endTime The new value for endTime
   * @param hashCode The new value for hashCode
   */
  public SpanDynamic(java.lang.String landscapeToken, java.lang.String spanId, java.lang.String parentSpanId, java.lang.String traceId, net.explorviz.avro.Timestamp startTime, net.explorviz.avro.Timestamp endTime, java.lang.String hashCode) {
    this.landscapeToken = landscapeToken;
    this.spanId = spanId;
    this.parentSpanId = parentSpanId;
    this.traceId = traceId;
    this.startTime = startTime;
    this.endTime = endTime;
    this.hashCode = hashCode;
  }

  public org.apache.avro.specific.SpecificData getSpecificData() { return MODEL$; }
  public org.apache.avro.Schema getSchema() { return SCHEMA$; }
  // Used by DatumWriter.  Applications should not call.
  public java.lang.Object get(int field$) {
    switch (field$) {
    case 0: return landscapeToken;
    case 1: return spanId;
    case 2: return parentSpanId;
    case 3: return traceId;
    case 4: return startTime;
    case 5: return endTime;
    case 6: return hashCode;
    default: throw new IndexOutOfBoundsException("Invalid index: " + field$);
    }
  }

  // Used by DatumReader.  Applications should not call.
  @SuppressWarnings(value="unchecked")
  public void put(int field$, java.lang.Object value$) {
    switch (field$) {
    case 0: landscapeToken = value$ != null ? value$.toString() : null; break;
    case 1: spanId = value$ != null ? value$.toString() : null; break;
    case 2: parentSpanId = value$ != null ? value$.toString() : null; break;
    case 3: traceId = value$ != null ? value$.toString() : null; break;
    case 4: startTime = (net.explorviz.avro.Timestamp)value$; break;
    case 5: endTime = (net.explorviz.avro.Timestamp)value$; break;
    case 6: hashCode = value$ != null ? value$.toString() : null; break;
    default: throw new IndexOutOfBoundsException("Invalid index: " + field$);
    }
  }

  /**
   * Gets the value of the 'landscapeToken' field.
   * @return The value of the 'landscapeToken' field.
   */
  public java.lang.String getLandscapeToken() {
    return landscapeToken;
  }


  /**
   * Sets the value of the 'landscapeToken' field.
   * @param value the value to set.
   */
  public void setLandscapeToken(java.lang.String value) {
    this.landscapeToken = value;
  }

  /**
   * Gets the value of the 'spanId' field.
   * @return The value of the 'spanId' field.
   */
  public java.lang.String getSpanId() {
    return spanId;
  }


  /**
   * Sets the value of the 'spanId' field.
   * @param value the value to set.
   */
  public void setSpanId(java.lang.String value) {
    this.spanId = value;
  }

  /**
   * Gets the value of the 'parentSpanId' field.
   * @return The value of the 'parentSpanId' field.
   */
  public java.lang.String getParentSpanId() {
    return parentSpanId;
  }


  /**
   * Sets the value of the 'parentSpanId' field.
   * @param value the value to set.
   */
  public void setParentSpanId(java.lang.String value) {
    this.parentSpanId = value;
  }

  /**
   * Gets the value of the 'traceId' field.
   * @return The value of the 'traceId' field.
   */
  public java.lang.String getTraceId() {
    return traceId;
  }


  /**
   * Sets the value of the 'traceId' field.
   * @param value the value to set.
   */
  public void setTraceId(java.lang.String value) {
    this.traceId = value;
  }

  /**
   * Gets the value of the 'startTime' field.
   * @return The value of the 'startTime' field.
   */
  public net.explorviz.avro.Timestamp getStartTime() {
    return startTime;
  }


  /**
   * Sets the value of the 'startTime' field.
   * @param value the value to set.
   */
  public void setStartTime(net.explorviz.avro.Timestamp value) {
    this.startTime = value;
  }

  /**
   * Gets the value of the 'endTime' field.
   * @return The value of the 'endTime' field.
   */
  public net.explorviz.avro.Timestamp getEndTime() {
    return endTime;
  }


  /**
   * Sets the value of the 'endTime' field.
   * @param value the value to set.
   */
  public void setEndTime(net.explorviz.avro.Timestamp value) {
    this.endTime = value;
  }

  /**
   * Gets the value of the 'hashCode' field.
   * @return The value of the 'hashCode' field.
   */
  public java.lang.String getHashCode() {
    return hashCode;
  }


  /**
   * Sets the value of the 'hashCode' field.
   * @param value the value to set.
   */
  public void setHashCode(java.lang.String value) {
    this.hashCode = value;
  }

  /**
   * Creates a new SpanDynamic RecordBuilder.
   * @return A new SpanDynamic RecordBuilder
   */
  public static net.explorviz.avro.SpanDynamic.Builder newBuilder() {
    return new net.explorviz.avro.SpanDynamic.Builder();
  }

  /**
   * Creates a new SpanDynamic RecordBuilder by copying an existing Builder.
   * @param other The existing builder to copy.
   * @return A new SpanDynamic RecordBuilder
   */
  public static net.explorviz.avro.SpanDynamic.Builder newBuilder(net.explorviz.avro.SpanDynamic.Builder other) {
    if (other == null) {
      return new net.explorviz.avro.SpanDynamic.Builder();
    } else {
      return new net.explorviz.avro.SpanDynamic.Builder(other);
    }
  }

  /**
   * Creates a new SpanDynamic RecordBuilder by copying an existing SpanDynamic instance.
   * @param other The existing instance to copy.
   * @return A new SpanDynamic RecordBuilder
   */
  public static net.explorviz.avro.SpanDynamic.Builder newBuilder(net.explorviz.avro.SpanDynamic other) {
    if (other == null) {
      return new net.explorviz.avro.SpanDynamic.Builder();
    } else {
      return new net.explorviz.avro.SpanDynamic.Builder(other);
    }
  }

  /**
   * RecordBuilder for SpanDynamic instances.
   */
  @org.apache.avro.specific.AvroGenerated
  public static class Builder extends org.apache.avro.specific.SpecificRecordBuilderBase<SpanDynamic>
    implements org.apache.avro.data.RecordBuilder<SpanDynamic> {

    private java.lang.String landscapeToken;
    private java.lang.String spanId;
    private java.lang.String parentSpanId;
    private java.lang.String traceId;
    private net.explorviz.avro.Timestamp startTime;
    private net.explorviz.avro.Timestamp.Builder startTimeBuilder;
    private net.explorviz.avro.Timestamp endTime;
    private net.explorviz.avro.Timestamp.Builder endTimeBuilder;
    private java.lang.String hashCode;

    /** Creates a new Builder */
    private Builder() {
      super(SCHEMA$, MODEL$);
    }

    /**
     * Creates a Builder by copying an existing Builder.
     * @param other The existing Builder to copy.
     */
    private Builder(net.explorviz.avro.SpanDynamic.Builder other) {
      super(other);
      if (isValidValue(fields()[0], other.landscapeToken)) {
        this.landscapeToken = data().deepCopy(fields()[0].schema(), other.landscapeToken);
        fieldSetFlags()[0] = other.fieldSetFlags()[0];
      }
      if (isValidValue(fields()[1], other.spanId)) {
        this.spanId = data().deepCopy(fields()[1].schema(), other.spanId);
        fieldSetFlags()[1] = other.fieldSetFlags()[1];
      }
      if (isValidValue(fields()[2], other.parentSpanId)) {
        this.parentSpanId = data().deepCopy(fields()[2].schema(), other.parentSpanId);
        fieldSetFlags()[2] = other.fieldSetFlags()[2];
      }
      if (isValidValue(fields()[3], other.traceId)) {
        this.traceId = data().deepCopy(fields()[3].schema(), other.traceId);
        fieldSetFlags()[3] = other.fieldSetFlags()[3];
      }
      if (isValidValue(fields()[4], other.startTime)) {
        this.startTime = data().deepCopy(fields()[4].schema(), other.startTime);
        fieldSetFlags()[4] = other.fieldSetFlags()[4];
      }
      if (other.hasStartTimeBuilder()) {
        this.startTimeBuilder = net.explorviz.avro.Timestamp.newBuilder(other.getStartTimeBuilder());
      }
      if (isValidValue(fields()[5], other.endTime)) {
        this.endTime = data().deepCopy(fields()[5].schema(), other.endTime);
        fieldSetFlags()[5] = other.fieldSetFlags()[5];
      }
      if (other.hasEndTimeBuilder()) {
        this.endTimeBuilder = net.explorviz.avro.Timestamp.newBuilder(other.getEndTimeBuilder());
      }
      if (isValidValue(fields()[6], other.hashCode)) {
        this.hashCode = data().deepCopy(fields()[6].schema(), other.hashCode);
        fieldSetFlags()[6] = other.fieldSetFlags()[6];
      }
    }

    /**
     * Creates a Builder by copying an existing SpanDynamic instance
     * @param other The existing instance to copy.
     */
    private Builder(net.explorviz.avro.SpanDynamic other) {
      super(SCHEMA$, MODEL$);
      if (isValidValue(fields()[0], other.landscapeToken)) {
        this.landscapeToken = data().deepCopy(fields()[0].schema(), other.landscapeToken);
        fieldSetFlags()[0] = true;
      }
      if (isValidValue(fields()[1], other.spanId)) {
        this.spanId = data().deepCopy(fields()[1].schema(), other.spanId);
        fieldSetFlags()[1] = true;
      }
      if (isValidValue(fields()[2], other.parentSpanId)) {
        this.parentSpanId = data().deepCopy(fields()[2].schema(), other.parentSpanId);
        fieldSetFlags()[2] = true;
      }
      if (isValidValue(fields()[3], other.traceId)) {
        this.traceId = data().deepCopy(fields()[3].schema(), other.traceId);
        fieldSetFlags()[3] = true;
      }
      if (isValidValue(fields()[4], other.startTime)) {
        this.startTime = data().deepCopy(fields()[4].schema(), other.startTime);
        fieldSetFlags()[4] = true;
      }
      this.startTimeBuilder = null;
      if (isValidValue(fields()[5], other.endTime)) {
        this.endTime = data().deepCopy(fields()[5].schema(), other.endTime);
        fieldSetFlags()[5] = true;
      }
      this.endTimeBuilder = null;
      if (isValidValue(fields()[6], other.hashCode)) {
        this.hashCode = data().deepCopy(fields()[6].schema(), other.hashCode);
        fieldSetFlags()[6] = true;
      }
    }

    /**
      * Gets the value of the 'landscapeToken' field.
      * @return The value.
      */
    public java.lang.String getLandscapeToken() {
      return landscapeToken;
    }


    /**
      * Sets the value of the 'landscapeToken' field.
      * @param value The value of 'landscapeToken'.
      * @return This builder.
      */
    public net.explorviz.avro.SpanDynamic.Builder setLandscapeToken(java.lang.String value) {
      validate(fields()[0], value);
      this.landscapeToken = value;
      fieldSetFlags()[0] = true;
      return this;
    }

    /**
      * Checks whether the 'landscapeToken' field has been set.
      * @return True if the 'landscapeToken' field has been set, false otherwise.
      */
    public boolean hasLandscapeToken() {
      return fieldSetFlags()[0];
    }


    /**
      * Clears the value of the 'landscapeToken' field.
      * @return This builder.
      */
    public net.explorviz.avro.SpanDynamic.Builder clearLandscapeToken() {
      landscapeToken = null;
      fieldSetFlags()[0] = false;
      return this;
    }

    /**
      * Gets the value of the 'spanId' field.
      * @return The value.
      */
    public java.lang.String getSpanId() {
      return spanId;
    }


    /**
      * Sets the value of the 'spanId' field.
      * @param value The value of 'spanId'.
      * @return This builder.
      */
    public net.explorviz.avro.SpanDynamic.Builder setSpanId(java.lang.String value) {
      validate(fields()[1], value);
      this.spanId = value;
      fieldSetFlags()[1] = true;
      return this;
    }

    /**
      * Checks whether the 'spanId' field has been set.
      * @return True if the 'spanId' field has been set, false otherwise.
      */
    public boolean hasSpanId() {
      return fieldSetFlags()[1];
    }


    /**
      * Clears the value of the 'spanId' field.
      * @return This builder.
      */
    public net.explorviz.avro.SpanDynamic.Builder clearSpanId() {
      spanId = null;
      fieldSetFlags()[1] = false;
      return this;
    }

    /**
      * Gets the value of the 'parentSpanId' field.
      * @return The value.
      */
    public java.lang.String getParentSpanId() {
      return parentSpanId;
    }


    /**
      * Sets the value of the 'parentSpanId' field.
      * @param value The value of 'parentSpanId'.
      * @return This builder.
      */
    public net.explorviz.avro.SpanDynamic.Builder setParentSpanId(java.lang.String value) {
      validate(fields()[2], value);
      this.parentSpanId = value;
      fieldSetFlags()[2] = true;
      return this;
    }

    /**
      * Checks whether the 'parentSpanId' field has been set.
      * @return True if the 'parentSpanId' field has been set, false otherwise.
      */
    public boolean hasParentSpanId() {
      return fieldSetFlags()[2];
    }


    /**
      * Clears the value of the 'parentSpanId' field.
      * @return This builder.
      */
    public net.explorviz.avro.SpanDynamic.Builder clearParentSpanId() {
      parentSpanId = null;
      fieldSetFlags()[2] = false;
      return this;
    }

    /**
      * Gets the value of the 'traceId' field.
      * @return The value.
      */
    public java.lang.String getTraceId() {
      return traceId;
    }


    /**
      * Sets the value of the 'traceId' field.
      * @param value The value of 'traceId'.
      * @return This builder.
      */
    public net.explorviz.avro.SpanDynamic.Builder setTraceId(java.lang.String value) {
      validate(fields()[3], value);
      this.traceId = value;
      fieldSetFlags()[3] = true;
      return this;
    }

    /**
      * Checks whether the 'traceId' field has been set.
      * @return True if the 'traceId' field has been set, false otherwise.
      */
    public boolean hasTraceId() {
      return fieldSetFlags()[3];
    }


    /**
      * Clears the value of the 'traceId' field.
      * @return This builder.
      */
    public net.explorviz.avro.SpanDynamic.Builder clearTraceId() {
      traceId = null;
      fieldSetFlags()[3] = false;
      return this;
    }

    /**
      * Gets the value of the 'startTime' field.
      * @return The value.
      */
    public net.explorviz.avro.Timestamp getStartTime() {
      return startTime;
    }


    /**
      * Sets the value of the 'startTime' field.
      * @param value The value of 'startTime'.
      * @return This builder.
      */
    public net.explorviz.avro.SpanDynamic.Builder setStartTime(net.explorviz.avro.Timestamp value) {
      validate(fields()[4], value);
      this.startTimeBuilder = null;
      this.startTime = value;
      fieldSetFlags()[4] = true;
      return this;
    }

    /**
      * Checks whether the 'startTime' field has been set.
      * @return True if the 'startTime' field has been set, false otherwise.
      */
    public boolean hasStartTime() {
      return fieldSetFlags()[4];
    }

    /**
     * Gets the Builder instance for the 'startTime' field and creates one if it doesn't exist yet.
     * @return This builder.
     */
    public net.explorviz.avro.Timestamp.Builder getStartTimeBuilder() {
      if (startTimeBuilder == null) {
        if (hasStartTime()) {
          setStartTimeBuilder(net.explorviz.avro.Timestamp.newBuilder(startTime));
        } else {
          setStartTimeBuilder(net.explorviz.avro.Timestamp.newBuilder());
        }
      }
      return startTimeBuilder;
    }

    /**
     * Sets the Builder instance for the 'startTime' field
     * @param value The builder instance that must be set.
     * @return This builder.
     */

    public net.explorviz.avro.SpanDynamic.Builder setStartTimeBuilder(net.explorviz.avro.Timestamp.Builder value) {
      clearStartTime();
      startTimeBuilder = value;
      return this;
    }

    /**
     * Checks whether the 'startTime' field has an active Builder instance
     * @return True if the 'startTime' field has an active Builder instance
     */
    public boolean hasStartTimeBuilder() {
      return startTimeBuilder != null;
    }

    /**
      * Clears the value of the 'startTime' field.
      * @return This builder.
      */
    public net.explorviz.avro.SpanDynamic.Builder clearStartTime() {
      startTime = null;
      startTimeBuilder = null;
      fieldSetFlags()[4] = false;
      return this;
    }

    /**
      * Gets the value of the 'endTime' field.
      * @return The value.
      */
    public net.explorviz.avro.Timestamp getEndTime() {
      return endTime;
    }


    /**
      * Sets the value of the 'endTime' field.
      * @param value The value of 'endTime'.
      * @return This builder.
      */
    public net.explorviz.avro.SpanDynamic.Builder setEndTime(net.explorviz.avro.Timestamp value) {
      validate(fields()[5], value);
      this.endTimeBuilder = null;
      this.endTime = value;
      fieldSetFlags()[5] = true;
      return this;
    }

    /**
      * Checks whether the 'endTime' field has been set.
      * @return True if the 'endTime' field has been set, false otherwise.
      */
    public boolean hasEndTime() {
      return fieldSetFlags()[5];
    }

    /**
     * Gets the Builder instance for the 'endTime' field and creates one if it doesn't exist yet.
     * @return This builder.
     */
    public net.explorviz.avro.Timestamp.Builder getEndTimeBuilder() {
      if (endTimeBuilder == null) {
        if (hasEndTime()) {
          setEndTimeBuilder(net.explorviz.avro.Timestamp.newBuilder(endTime));
        } else {
          setEndTimeBuilder(net.explorviz.avro.Timestamp.newBuilder());
        }
      }
      return endTimeBuilder;
    }

    /**
     * Sets the Builder instance for the 'endTime' field
     * @param value The builder instance that must be set.
     * @return This builder.
     */

    public net.explorviz.avro.SpanDynamic.Builder setEndTimeBuilder(net.explorviz.avro.Timestamp.Builder value) {
      clearEndTime();
      endTimeBuilder = value;
      return this;
    }

    /**
     * Checks whether the 'endTime' field has an active Builder instance
     * @return True if the 'endTime' field has an active Builder instance
     */
    public boolean hasEndTimeBuilder() {
      return endTimeBuilder != null;
    }

    /**
      * Clears the value of the 'endTime' field.
      * @return This builder.
      */
    public net.explorviz.avro.SpanDynamic.Builder clearEndTime() {
      endTime = null;
      endTimeBuilder = null;
      fieldSetFlags()[5] = false;
      return this;
    }

    /**
      * Gets the value of the 'hashCode' field.
      * @return The value.
      */
    public java.lang.String getHashCode() {
      return hashCode;
    }


    /**
      * Sets the value of the 'hashCode' field.
      * @param value The value of 'hashCode'.
      * @return This builder.
      */
    public net.explorviz.avro.SpanDynamic.Builder setHashCode(java.lang.String value) {
      validate(fields()[6], value);
      this.hashCode = value;
      fieldSetFlags()[6] = true;
      return this;
    }

    /**
      * Checks whether the 'hashCode' field has been set.
      * @return True if the 'hashCode' field has been set, false otherwise.
      */
    public boolean hasHashCode() {
      return fieldSetFlags()[6];
    }


    /**
      * Clears the value of the 'hashCode' field.
      * @return This builder.
      */
    public net.explorviz.avro.SpanDynamic.Builder clearHashCode() {
      hashCode = null;
      fieldSetFlags()[6] = false;
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    public SpanDynamic build() {
      try {
        SpanDynamic record = new SpanDynamic();
        record.landscapeToken = fieldSetFlags()[0] ? this.landscapeToken : (java.lang.String) defaultValue(fields()[0]);
        record.spanId = fieldSetFlags()[1] ? this.spanId : (java.lang.String) defaultValue(fields()[1]);
        record.parentSpanId = fieldSetFlags()[2] ? this.parentSpanId : (java.lang.String) defaultValue(fields()[2]);
        record.traceId = fieldSetFlags()[3] ? this.traceId : (java.lang.String) defaultValue(fields()[3]);
        if (startTimeBuilder != null) {
          try {
            record.startTime = this.startTimeBuilder.build();
          } catch (org.apache.avro.AvroMissingFieldException e) {
            e.addParentField(record.getSchema().getField("startTime"));
            throw e;
          }
        } else {
          record.startTime = fieldSetFlags()[4] ? this.startTime : (net.explorviz.avro.Timestamp) defaultValue(fields()[4]);
        }
        if (endTimeBuilder != null) {
          try {
            record.endTime = this.endTimeBuilder.build();
          } catch (org.apache.avro.AvroMissingFieldException e) {
            e.addParentField(record.getSchema().getField("endTime"));
            throw e;
          }
        } else {
          record.endTime = fieldSetFlags()[5] ? this.endTime : (net.explorviz.avro.Timestamp) defaultValue(fields()[5]);
        }
        record.hashCode = fieldSetFlags()[6] ? this.hashCode : (java.lang.String) defaultValue(fields()[6]);
        return record;
      } catch (org.apache.avro.AvroMissingFieldException e) {
        throw e;
      } catch (java.lang.Exception e) {
        throw new org.apache.avro.AvroRuntimeException(e);
      }
    }
  }

  @SuppressWarnings("unchecked")
  private static final org.apache.avro.io.DatumWriter<SpanDynamic>
    WRITER$ = (org.apache.avro.io.DatumWriter<SpanDynamic>)MODEL$.createDatumWriter(SCHEMA$);

  @Override public void writeExternal(java.io.ObjectOutput out)
    throws java.io.IOException {
    WRITER$.write(this, SpecificData.getEncoder(out));
  }

  @SuppressWarnings("unchecked")
  private static final org.apache.avro.io.DatumReader<SpanDynamic>
    READER$ = (org.apache.avro.io.DatumReader<SpanDynamic>)MODEL$.createDatumReader(SCHEMA$);

  @Override public void readExternal(java.io.ObjectInput in)
    throws java.io.IOException {
    READER$.read(this, SpecificData.getDecoder(in));
  }

  @Override protected boolean hasCustomCoders() { return true; }

  @Override public void customEncode(org.apache.avro.io.Encoder out)
    throws java.io.IOException
  {
    out.writeString(this.landscapeToken);

    out.writeString(this.spanId);

    out.writeString(this.parentSpanId);

    out.writeString(this.traceId);

    this.startTime.customEncode(out);

    this.endTime.customEncode(out);

    out.writeString(this.hashCode);

  }

  @Override public void customDecode(org.apache.avro.io.ResolvingDecoder in)
    throws java.io.IOException
  {
    org.apache.avro.Schema.Field[] fieldOrder = in.readFieldOrderIfDiff();
    if (fieldOrder == null) {
      this.landscapeToken = in.readString();

      this.spanId = in.readString();

      this.parentSpanId = in.readString();

      this.traceId = in.readString();

      if (this.startTime == null) {
        this.startTime = new net.explorviz.avro.Timestamp();
      }
      this.startTime.customDecode(in);

      if (this.endTime == null) {
        this.endTime = new net.explorviz.avro.Timestamp();
      }
      this.endTime.customDecode(in);

      this.hashCode = in.readString();

    } else {
      for (int i = 0; i < 7; i++) {
        switch (fieldOrder[i].pos()) {
        case 0:
          this.landscapeToken = in.readString();
          break;

        case 1:
          this.spanId = in.readString();
          break;

        case 2:
          this.parentSpanId = in.readString();
          break;

        case 3:
          this.traceId = in.readString();
          break;

        case 4:
          if (this.startTime == null) {
            this.startTime = new net.explorviz.avro.Timestamp();
          }
          this.startTime.customDecode(in);
          break;

        case 5:
          if (this.endTime == null) {
            this.endTime = new net.explorviz.avro.Timestamp();
          }
          this.endTime.customDecode(in);
          break;

        case 6:
          this.hashCode = in.readString();
          break;

        default:
          throw new java.io.IOException("Corrupt ResolvingDecoder.");
        }
      }
    }
  }
}










