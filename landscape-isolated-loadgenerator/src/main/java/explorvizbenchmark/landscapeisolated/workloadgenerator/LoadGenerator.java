package explorvizbenchmark.landscapeisolated.workloadgenerator;

import java.io.IOException;
import java.time.Duration;
import java.util.Objects;
import java.util.Properties;

import io.opencensus.proto.dump.DumpSpans;
import net.explorviz.avro.Timestamp;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import theodolite.commons.workloadgeneration.*;

/**
 * Load Generator for Theodolite use case UC1.
 */
public final class LoadGenerator {

  private static final Logger LOGGER = LoggerFactory.getLogger(LoadGenerator.class);

  private static final String BOOTSTRAP_SERVER_DEFAULT = "localhost:5701";
  private static final String SENSOR_PREFIX_DEFAULT = "";
  private static final int NUMBER_OF_KEYS_DEFAULT = 10;
  private static final int PERIOD_MS_DEFAULT = 1000;
  private static final int VALUE_DEFAULT = 10;
  private static final int THREADS_DEFAULT = 4;
  private static final String SCHEMA_REGISTRY_URL_DEFAULT = "http://localhost:8081";
  private static final String KAFKA_TOPIC_DEFAULT = "input";
  private static final String KAFKA_BOOTSTRAP_SERVERS_DEFAULT = "localhost:9092"; // NOPMD
  private static final String LANDSCAPE_TOKEN_DEFAULT = "9dcb88d3-69c7-4dc9-90dc-5d1899ea8aff";
  private static final String TOKEN_SECRET_DEFAULT = "gC7YFkn2UEv0atff";
  private static final int SPANS_AMOUNT_DEFAULT = 10;

  private LoadGenerator() {}

  public static theodolite.commons.workloadgeneration.LoadGenerator fromEnvironment() {
    final String bootstrapServer = System.getenv(ConfigurationKeys.BOOTSTRAP_SERVER);
    final String kubernetesDnsName = System.getenv(ConfigurationKeys.KUBERNETES_DNS_NAME);

    ClusterConfig clusterConfig;
    if (bootstrapServer != null) { // NOPMD
      clusterConfig = ClusterConfig.fromBootstrapServer(bootstrapServer);
      LOGGER.info("Use bootstrap server '{}'.", bootstrapServer);
    } else if (kubernetesDnsName != null) { // NOPMD
      clusterConfig = ClusterConfig.fromKubernetesDnsName(kubernetesDnsName);
      LOGGER.info("Use Kubernetes DNS name '{}'.", kubernetesDnsName);
    } else {
      clusterConfig = ClusterConfig.fromBootstrapServer(BOOTSTRAP_SERVER_DEFAULT);
      LOGGER.info(
              "Neither a bootstrap server nor a Kubernetes DNS name was provided. Use default bootstrap server '{}'.", // NOCS
              BOOTSTRAP_SERVER_DEFAULT);
    }

    final String port = System.getenv(ConfigurationKeys.PORT);
    if (port != null) {
      clusterConfig.setPort(Integer.parseInt(port));
    }

    final String portAutoIncrement = System.getenv(ConfigurationKeys.PORT_AUTO_INCREMENT);
    if (portAutoIncrement != null) {
      clusterConfig.setPortAutoIncrement(Boolean.parseBoolean(portAutoIncrement));
    }

    final String clusterNamePrefix = System.getenv(ConfigurationKeys.CLUSTER_NAME_PREFIX);
    if (clusterNamePrefix != null) {
      clusterConfig.setClusterNamePrefix(portAutoIncrement);
    }

    final String kafkaBootstrapServers = Objects.requireNonNullElse(
            System.getenv(ConfigurationKeys.KAFKA_BOOTSTRAP_SERVERS),
            KAFKA_BOOTSTRAP_SERVERS_DEFAULT);
    final String kafkaInputTopic = Objects.requireNonNullElse(
            System.getenv(ConfigurationKeys.KAFKA_INPUT_TOPIC),
            KAFKA_TOPIC_DEFAULT);
    final String schemaRegistryUrl = Objects.requireNonNullElse(
            System.getenv(ConfigurationKeys.SCHEMA_REGISTRY_URL),
            SCHEMA_REGISTRY_URL_DEFAULT);
    final Properties kafkaProperties = new Properties();
    kafkaProperties.compute(ProducerConfig.BATCH_SIZE_CONFIG,
            (k, v) -> System.getenv(ConfigurationKeys.KAFKA_BATCH_SIZE));
    kafkaProperties.compute(ProducerConfig.LINGER_MS_CONFIG,
            (k, v) -> System.getenv(ConfigurationKeys.KAFKA_LINGER_MS));
    kafkaProperties.compute(ProducerConfig.BUFFER_MEMORY_CONFIG,
            (k, v) -> System.getenv(ConfigurationKeys.KAFKA_BUFFER_MEMORY));
    final RecordSender<DumpSpans> recordSender = KafkaSpanStructureRecordSender
            .<DumpSpans>builder(kafkaBootstrapServers, kafkaInputTopic, schemaRegistryUrl)
            .keyAccessor(r -> r.getSpanId())
            .timestampAccessor(r -> {
              Timestamp startTime = r.getTimestamp();
              return (startTime.getSeconds() * 1000) + (startTime.getNanoAdjust() / 1_000_000);
            })
            .build();
    LOGGER.info(
            "Use Kafka as target with bootstrap server '{}', schema registry url '{}' and topic '{}'.", // NOCS
            kafkaBootstrapServers, schemaRegistryUrl, kafkaInputTopic);

    final int numSensors = Integer.parseInt(Objects.requireNonNullElse(
            System.getenv(ConfigurationKeys.NUM_SENSORS),
            Integer.toString(NUMBER_OF_KEYS_DEFAULT)));
    final int periodMs = Integer.parseInt(Objects.requireNonNullElse(
            System.getenv(ConfigurationKeys.PERIOD_MS),
            Integer.toString(PERIOD_MS_DEFAULT)));
    final double value = Double.parseDouble(Objects.requireNonNullElse(
            System.getenv(ConfigurationKeys.VALUE),
            Integer.toString(VALUE_DEFAULT)));
    final int threads = Integer.parseInt(Objects.requireNonNullElse(
            System.getenv(ConfigurationKeys.THREADS),
            Integer.toString(THREADS_DEFAULT)));

    final String landscapeToken = Objects.requireNonNullElse(System.getenv("LANDSCAPE_TOKEN"), LANDSCAPE_TOKEN_DEFAULT);
    final String tokenSecret = Objects.requireNonNullElse(System.getenv("TOKEN_SECRET"), TOKEN_SECRET_DEFAULT);
    final int spansAmount = Integer.parseInt(Objects.requireNonNullElse(System.getenv("SPANS_AMOUNT"), Integer.toString(SPANS_AMOUNT_DEFAULT)));

    return new theodolite.commons.workloadgeneration.LoadGenerator()
            .setClusterConfig(clusterConfig)
            .setLoadDefinition(new WorkloadDefinition(
                    new KeySpace(SENSOR_PREFIX_DEFAULT, numSensors),
                    Duration.ofMillis(periodMs)))
            .setGeneratorConfig(new LoadGeneratorConfig(
                    new DumpSpansRecordGenerator(spansAmount, landscapeToken, tokenSecret),
                    recordSender))
            .withThreads(threads);
  }

  /**
   * Start load generator for use case UC1.
   */
  public static void main(final String[] args) throws InterruptedException, IOException {
    LOGGER.info("Start workload generator for use case UC1.");
    explorvizbenchmark.landscapeisolated.workloadgenerator.LoadGenerator.fromEnvironment().run();
  }
}
