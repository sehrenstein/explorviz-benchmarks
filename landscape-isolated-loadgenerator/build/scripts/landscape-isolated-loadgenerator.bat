@rem
@rem Copyright 2015 the original author or authors.
@rem
@rem Licensed under the Apache License, Version 2.0 (the "License");
@rem you may not use this file except in compliance with the License.
@rem You may obtain a copy of the License at
@rem
@rem      https://www.apache.org/licenses/LICENSE-2.0
@rem
@rem Unless required by applicable law or agreed to in writing, software
@rem distributed under the License is distributed on an "AS IS" BASIS,
@rem WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
@rem See the License for the specific language governing permissions and
@rem limitations under the License.
@rem

@if "%DEBUG%" == "" @echo off
@rem ##########################################################################
@rem
@rem  landscape-isolated-loadgenerator startup script for Windows
@rem
@rem ##########################################################################

@rem Set local scope for the variables with windows NT shell
if "%OS%"=="Windows_NT" setlocal

set DIRNAME=%~dp0
if "%DIRNAME%" == "" set DIRNAME=.
set APP_BASE_NAME=%~n0
set APP_HOME=%DIRNAME%..

@rem Resolve any "." and ".." in APP_HOME to make it shorter.
for %%i in ("%APP_HOME%") do set APP_HOME=%%~fi

@rem Add default JVM options here. You can also use JAVA_OPTS and LANDSCAPE_ISOLATED_LOADGENERATOR_OPTS to pass JVM options to this script.
set DEFAULT_JVM_OPTS=

@rem Find java.exe
if defined JAVA_HOME goto findJavaFromJavaHome

set JAVA_EXE=java.exe
%JAVA_EXE% -version >NUL 2>&1
if "%ERRORLEVEL%" == "0" goto execute

echo.
echo ERROR: JAVA_HOME is not set and no 'java' command could be found in your PATH.
echo.
echo Please set the JAVA_HOME variable in your environment to match the
echo location of your Java installation.

goto fail

:findJavaFromJavaHome
set JAVA_HOME=%JAVA_HOME:"=%
set JAVA_EXE=%JAVA_HOME%/bin/java.exe

if exist "%JAVA_EXE%" goto execute

echo.
echo ERROR: JAVA_HOME is set to an invalid directory: %JAVA_HOME%
echo.
echo Please set the JAVA_HOME variable in your environment to match the
echo location of your Java installation.

goto fail

:execute
@rem Setup the command line

set CLASSPATH=%APP_HOME%\lib\landscape-isolated-loadgenerator.jar;%APP_HOME%\lib\load-generator-commons.jar;%APP_HOME%\lib\titan-ccp-common-0.1.0-SNAPSHOT.jar;%APP_HOME%\lib\titan-ccp-common-kafka-0.1.0-SNAPSHOT.jar;%APP_HOME%\lib\slf4j-simple-1.7.25.jar;%APP_HOME%\lib\opencensus-proto-0.2.0.jar;%APP_HOME%\lib\kafka-streams-2.6.0.jar;%APP_HOME%\lib\kafka-streams-avro-serde-5.5.0.jar;%APP_HOME%\lib\kafka-avro-serializer-5.5.0.jar;%APP_HOME%\lib\kafka-schema-serializer-5.5.0.jar;%APP_HOME%\lib\kafka-schema-registry-client-5.5.0.jar;%APP_HOME%\lib\avro-1.11.0.jar;%APP_HOME%\lib\connect-json-2.6.0.jar;%APP_HOME%\lib\connect-api-2.6.0.jar;%APP_HOME%\lib\kafka-clients-5.5.0-ccs.jar;%APP_HOME%\lib\commons-configuration2-2.0.jar;%APP_HOME%\lib\commons-beanutils-1.9.2.jar;%APP_HOME%\lib\grpc-protobuf-1.14.0.jar;%APP_HOME%\lib\grpc-stub-1.14.0.jar;%APP_HOME%\lib\grpc-protobuf-lite-1.14.0.jar;%APP_HOME%\lib\grpc-core-1.14.0.jar;%APP_HOME%\lib\gson-2.8.2.jar;%APP_HOME%\lib\swagger-core-1.5.3.jar;%APP_HOME%\lib\guava-30.1-jre.jar;%APP_HOME%\lib\common-config-5.5.0.jar;%APP_HOME%\lib\common-utils-5.5.0.jar;%APP_HOME%\lib\swagger-models-1.5.3.jar;%APP_HOME%\lib\slf4j-api-1.7.32.jar;%APP_HOME%\lib\hazelcast-4.1.1.jar;%APP_HOME%\lib\hazelcast-kubernetes-2.2.1.jar;%APP_HOME%\lib\protobuf-java-3.5.1.jar;%APP_HOME%\lib\rocksdbjni-5.18.4.jar;%APP_HOME%\lib\jackson-annotations-2.12.5.jar;%APP_HOME%\lib\jackson-datatype-jdk8-2.12.5.jar;%APP_HOME%\lib\jackson-dataformat-yaml-2.12.5.jar;%APP_HOME%\lib\jackson-databind-2.12.5.jar;%APP_HOME%\lib\jackson-datatype-joda-2.12.5.jar;%APP_HOME%\lib\jackson-core-2.12.5.jar;%APP_HOME%\lib\commons-compress-1.21.jar;%APP_HOME%\lib\commons-lang3-3.3.2.jar;%APP_HOME%\lib\commons-logging-1.2.jar;%APP_HOME%\lib\commons-collections-3.2.1.jar;%APP_HOME%\lib\failureaccess-1.0.1.jar;%APP_HOME%\lib\listenablefuture-9999.0-empty-to-avoid-conflict-with-guava.jar;%APP_HOME%\lib\jsr305-3.0.2.jar;%APP_HOME%\lib\checker-qual-3.5.0.jar;%APP_HOME%\lib\error_prone_annotations-2.3.4.jar;%APP_HOME%\lib\j2objc-annotations-1.3.jar;%APP_HOME%\lib\proto-google-common-protos-1.0.0.jar;%APP_HOME%\lib\zstd-jni-1.4.4-7.jar;%APP_HOME%\lib\lz4-java-1.7.1.jar;%APP_HOME%\lib\snappy-java-1.1.7.3.jar;%APP_HOME%\lib\jersey-bean-validation-2.30.jar;%APP_HOME%\lib\swagger-annotations-1.5.22.jar;%APP_HOME%\lib\grpc-context-1.14.0.jar;%APP_HOME%\lib\opencensus-contrib-grpc-metrics-0.12.3.jar;%APP_HOME%\lib\opencensus-api-0.12.3.jar;%APP_HOME%\lib\jersey-server-2.30.jar;%APP_HOME%\lib\jersey-client-2.30.jar;%APP_HOME%\lib\jersey-media-jaxb-2.30.jar;%APP_HOME%\lib\jersey-common-2.30.jar;%APP_HOME%\lib\jakarta.inject-2.6.1.jar;%APP_HOME%\lib\jakarta.validation-api-2.0.2.jar;%APP_HOME%\lib\hibernate-validator-6.0.17.Final.jar;%APP_HOME%\lib\jakarta.el-api-3.0.3.jar;%APP_HOME%\lib\jakarta.el-3.0.2.jar;%APP_HOME%\lib\jakarta.ws.rs-api-2.1.6.jar;%APP_HOME%\lib\jakarta.annotation-api-1.3.5.jar;%APP_HOME%\lib\osgi-resource-locator-1.0.3.jar;%APP_HOME%\lib\jboss-logging-3.3.2.Final.jar;%APP_HOME%\lib\classmate-1.3.4.jar;%APP_HOME%\lib\joda-time-2.10.8.jar;%APP_HOME%\lib\snakeyaml-1.27.jar


@rem Execute landscape-isolated-loadgenerator
"%JAVA_EXE%" %DEFAULT_JVM_OPTS% %JAVA_OPTS% %LANDSCAPE_ISOLATED_LOADGENERATOR_OPTS%  -classpath "%CLASSPATH%" explorvizbenchmark.landscapeisolated.workloadgenerator.LoadGenerator %*

:end
@rem End local scope for the variables with windows NT shell
if "%ERRORLEVEL%"=="0" goto mainEnd

:fail
rem Set variable LANDSCAPE_ISOLATED_LOADGENERATOR_EXIT_CONSOLE if you need the _script_ return code instead of
rem the _cmd.exe /c_ return code!
if  not "" == "%LANDSCAPE_ISOLATED_LOADGENERATOR_EXIT_CONSOLE%" exit 1
exit /b 1

:mainEnd
if "%OS%"=="Windows_NT" endlocal

:omega
