#!/bin/bash

echo "public class Main {" >> Main.java
echo "private static final long RATE = Long.parseLong(System.getenv(\"RATE\"));" >> Main.java

i=0
while [ $i -ne $1 ]
do
        i=$(($i+1))
        echo "private static int operation$i(final int depth) { System.out.println(\"called\"); return depth; }" >> Main.java
done

echo "public static void main(String[] args) {" >> Main.java
echo "while (true) {" >> Main.java

i=0
while [ $i -ne $1 ]
do
        i=$(($i+1))
        echo "operation$i(1);" >> Main.java
done
echo "try {Thread.sleep(RATE);} catch (InterruptedException e) {throw new RuntimeException(e);}" >> Main.java
echo "}" >> Main.java
echo "}" >> Main.java
echo "}" >> Main.java

javac Main.java
jar cfe app.jar Main Main.class