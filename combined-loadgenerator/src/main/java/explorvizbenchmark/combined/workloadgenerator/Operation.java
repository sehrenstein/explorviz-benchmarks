package explorvizbenchmark.combined.workloadgenerator;

public class Operation {
    public void performNTimes(final int n) {
        if (n > 0) performNTimes(n-1);
    }
}
