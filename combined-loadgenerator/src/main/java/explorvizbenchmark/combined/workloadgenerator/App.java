package explorvizbenchmark.combined.workloadgenerator;

import java.util.concurrent.*;

public class App {

    private static final long RATE = Long.parseLong(System.getenv("RATE"));
    private static final int THREADS = Integer.parseInt(System.getenv("THREADS"));
    private static final int RECURSION_DEPTH= Integer.parseInt(System.getenv("RECURSION_DEPTH"));

    public static void main(String[] args) {
        final Operation operation = new Operation();
        while (true) {
            operation.performNTimes(1);
            try {
                Thread.sleep(RATE);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
    /*public static void main(String[] args) {
        //ScheduledExecutorService executor = Executors.newScheduledThreadPool(THREADS);
        //executor.scheduleAtFixedRate(App::run, 0, RATE, TimeUnit.MILLISECONDS);
    }

    private static void run() {
        System.out.println("Operation performed.");
        App.recursiveOperation(RECURSION_DEPTH);
    }*/

    private static void recursiveOperation(final int depth) {
        if (depth > 0) recursiveOperation(depth-1);
    }
}
